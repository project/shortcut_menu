INTRODUCTION
============

This is a simple module that extends the functionality of the Drupal core
shortcut module.
The core shortcut module only allows for a single row of shortcut links.
By using this module, we gain the ability to
nest shortcut links under each other. Creating a similar experience as 
other menu forms.


REQUIREMENTS
============
This module has no requirements.


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
=============
1. No configuration required. Simply enable the module and edit the shortcuts 
as desired.
