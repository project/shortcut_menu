<?php

namespace Drupal\shortcut_menu;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\shortcut\ShortcutLazyBuilders;

class ShortcutMenuLazyBuilder extends ShortcutLazyBuilders {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The lazy builder service.
   *
   * @var \Drupal\shortcut\ShortcutLazyBuilders
   */
  protected $shortcutBuilder;

  /**
   * Shortcut menu lazy builder constructor.
   *
   * @param \Drupal\shortcut\ShortcutLazyBuilders $shortcut_builder
   *   Original lazy builder service.
   */
  public function __construct(ShortcutLazyBuilders $shortcut_builder, EntityTypeManagerInterface $entity_type_manager) {
    $this->shortcutBuilder = $shortcut_builder;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Using the original lazy builder service, build the shortcut menu.
   *
   * @return array
   *   Shortcut menu renderable array.
   */
  public function lazyLinks(bool $show_configure_link = TRUE): array {
    $links = $this->shortcutBuilder->lazyLinks();
    if (!isset($links['shortcuts']['#links'])) {
      return $links;
    }

    $shortcuts = $this->entityTypeManager->getStorage('shortcut')
      ->loadMultiple(array_keys($links['shortcuts']['#links']));

    // Find the max depth of the menu.
    $max_depth = 0;
    foreach ($shortcuts as $shortcut) {
      $shortcut_depth = $shortcut->get('depth')->getString();
      $max_depth = $shortcut_depth > $max_depth ? $shortcut_depth : $max_depth;
    }

    // Loop through the menu depths, constructing each level from the top down.
    while ($max_depth > 0) {

      foreach ($shortcuts as $shortcut) {
        $shortcut_depth = $shortcut->get('depth')->getString();
        $parent = $shortcut->get('parent')->getString();
        $parent = $this->getParentIdFromUuid($parent);

        if ($shortcut_depth == $max_depth && $parent && isset($links['shortcuts']['#links'][$parent])) {
          $links['shortcuts']['#links'][$shortcut->id()]['weight'] = (int) $shortcut->get('weight')
            ->getString();

          $links['shortcuts']['#links'][$parent]['below'][$shortcut->id()] = $links['shortcuts']['#links'][$shortcut->id()];

          uasort($links['shortcuts']['#links'][$parent]['below'], [
            SortArray::class,
            'sortByWeightElement',
          ]);
          unset($links['shortcuts']['#links'][$shortcut->id()]);
        }

      }
      $max_depth--;
    }

    $links['shortcuts']['#attached']['library'][] = 'shortcut_menu/toolbar';
    $links['shortcuts']['#theme'] = 'menu';
    $links['shortcuts']['#menu_name'] = 'shortcut_menu';
    $links['shortcuts']['#items'] = $links['shortcuts']['#links'];
    unset($links['shortcuts']['#links']);

    return $links;
  }

  protected function getParentIdFromUuid($uuid) {
    $shortcut = $this->entityTypeManager->getStorage('shortcut')
      ->loadByProperties(['uuid' => $uuid]);
    if ($shortcut) {
      return reset($shortcut)->id();
    }
    return NULL;
  }

}
