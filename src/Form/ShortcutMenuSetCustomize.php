<?php

namespace Drupal\shortcut_menu\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\shortcut\Form\SetCustomize;

/**
 * Class ShortcutMenuSetCustomize.
 *
 * @package Drupal\shortcut_menu\Form
 */
class ShortcutMenuSetCustomize extends SetCustomize {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['shortcuts']['links']['#tabledrag'] = $this->getTableDragSettings();
    $shortcuts = $this->entity->getShortcuts();

    foreach ($shortcuts as $shortcut) {
      if(!isset($form['shortcuts']['links'][$shortcut->id()]['name'])){
        continue;
      }

      $indentation = [];
      $parent_uuid = $shortcut->get('parent')->getString() ?: '';
      $parent = $this->getShortcutIdFromUuid($parent_uuid) ?: '';

      $depth = (int) $shortcut->get('depth')->getString();
      if ($depth > 0) {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => isset($shortcuts[$parent]) ? $depth : $depth - 1,
        ];
      }

      $first_column = &$form['shortcuts']['links'][$shortcut->id()]['name'];
      $first_column['#title'] = $shortcut->getTitle();

      $first_column['#prefix'] = !empty($indentation) ? \Drupal::service('renderer')
        ->render($indentation) : '';

      $first_column['shortcut_id'] = [
        '#type' => 'hidden',
        '#value' => $shortcut->id(),
        '#attributes' => ['class' => ['shortcut-id']],
      ];

      $first_column['parent'] = [
        '#type' => 'hidden',
        '#default_value' => isset($shortcuts[$parent]) ? $parent : 0,
        '#attributes' => ['class' => ['shortcut-parent']],
      ];
      $first_column['depth'] = [
        '#type' => 'hidden',
        '#default_value' => $depth,
        '#attributes' => ['class' => ['shortcut-depth']],
      ];
    }
    return $form;
  }

  /**
   * Get the table drag settings array.
   *
   * @return array
   *   Table drag settings.
   */
  protected function getTableDragSettings() {
    return [
      [
        'action' => 'match',
        'relationship' => 'parent',
        'group' => 'shortcut-parent',
        'subgroup' => 'shortcut-parent',
        'source' => 'shortcut-id',
        "hidden" => FALSE,
      ],
      [
        'action' => 'depth',
        'relationship' => 'group',
        'group' => 'shortcut-depth',
        'hidden' => FALSE,
      ],
      [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'shortcut-weight',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $link_values = $form_state->getValue(['shortcuts', 'links']);

    // Sort and set new link weights in the form values.
    $link_values = $this->sortLinkWeights($link_values);

    foreach ($this->entity->getShortcuts() as $shortcut) {
      $shortcut_values = $link_values[$shortcut->id()];
      $shortcut->set('parent', $this->getShortcutUuidFromId($shortcut_values['name']['parent']));
      $shortcut->setWeight($shortcut_values['weight']);
      $shortcut->set('depth', $shortcut_values['name']['depth']);
      $shortcut->save();
    }
    $this->messenger()
      ->addStatus($this->t('The shortcut set has been updated.'));
  }

  /**
   * Sort the submitted link values and give them appropriate weights.
   *
   * @param array $values
   *   Form state values.
   *
   * @return array
   *   Modified values with new weights.
   */
  protected function sortLinkWeights(array $values) {
    $sortable_array = [];
    foreach (array_keys($values) as $link_id) {
      $weight = $this->getRootWeight($values, $link_id);

      while (in_array($weight, $sortable_array)) {
        $weight += .001;
      }

      $sortable_array[$link_id] = $weight;
    }
    asort($sortable_array, SORT_NUMERIC);

    foreach (array_keys($sortable_array) as $new_weight => $link_id) {
      $values[$link_id]['weight'] = $new_weight;
    }
    return $values;
  }

  /**
   * Get the root parent weight of the provided link from the form.
   *
   * @param array $values
   *   Form submitted values.
   * @param int $link_id
   *   Shortcut Id.
   *
   * @return int
   *   Root parent weight.
   */
  protected function getRootWeight(array $values, $link_id) {
    if (!isset($values[$link_id])) {
      return 0;
    }
    if ($values[$link_id]['name']['parent']) {
      return $this->getRootWeight($values, $values[$link_id]['name']['parent']);
    }
    return $values[$link_id]['weight'];
  }

  /**
   * Get the id of the shortcut from the uuid.
   *
   * @param string $uuid
   *   Shortcut uuid.
   *
   * @return int|null
   *   Shortcut ID.
   */
  protected function getShortcutIdFromUuid(string $uuid = null): ?int {
    $shortcuts = $this->entity->getShortcuts();
    foreach ($shortcuts as $shortcut) {
      if ($shortcut->uuid() == $uuid) {
        return (int) $shortcut->id();
      }
    }
    return FALSE;
  }

  /**
   * Get the shortcut UUID using the id of the shortcut.
   *
   * @param int $id
   *   Entity id.
   *
   * @return string|null
   *   Entity uuid.
   */
  protected function getShortcutUuidFromId(int $id = null): ?string {
    $shortcuts = $this->entity->getShortcuts();
    foreach ($shortcuts as $shortcut) {
      if ($shortcut->id() == $id) {
        return $shortcut->uuid();
      }
    }
    return FALSE;
  }

}
